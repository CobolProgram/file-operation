       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. MMODPOWW.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE   ASSIGN TO   "emp1.dat"
              ORGANIZATION   IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETIALS.
           88 END-OF-EMP-FILE   VALUE  HIGH-VALUE.
           05 EMP-SSN  PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORNAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB  PIC 9(4).
              10 EMP-MOB  PIC 9(2).
              10 EMP-DOB  PIC 9(2).
           05 EMP-GENDER  PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN  OUTPUT EMP-FILE
           MOVE  "123456789" TO EMP-SSN
           MOVE  "PATTARAWAN"   TO EMP-SURNAME
           MOVE  "RASSAMEE"  TO   EMP-FORNAME  
           MOVE  "20010201"  TO EMP-DATE-OF-BIRTH
           MOVE  "F"   TO EMP-GENDER 
           WRITE EMP-DETIALS 

           MOVE  "987654321" TO EMP-SSN
           MOVE  "NATRUJA"   TO EMP-SURNAME
           MOVE  "MANNIN"  TO   EMP-FORNAME  
           MOVE  "20000524"  TO EMP-DATE-OF-BIRTH
           MOVE  "F"   TO EMP-GENDER 
           WRITE EMP-DETIALS
      
           MOVE  "192837465" TO EMP-SSN
           MOVE  "SIRARAT"   TO EMP-SURNAME
           MOVE  "YAIMADUER"  TO   EMP-FORNAME  
           MOVE  "20000721"  TO EMP-DATE-OF-BIRTH
           MOVE  "F"   TO EMP-GENDER 
           WRITE EMP-DETIALS
           CLOSE EMP-FILE 
           GOBACK 
           .