       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. MMODPOWW.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION   IS LINE  SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DDETAIL.
           05 STU-ID   PIC   9(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE    PIC   9(2)V9(2).
           05 PROJECT-SCORE    PIC   9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE 
           MOVE  "62160292"  TO STU-ID
           MOVE  "40"   TO MIDTERM-SCORE 
           MOVE  "40"  TO FINAL-SCORE 
           MOVE  "20"  TO PROJECT-SCORE 
           WRITE SCORE-DDETAIL

           MOVE  "62160262"  TO STU-ID
           MOVE  "20"   TO MIDTERM-SCORE 
           MOVE  "20"  TO FINAL-SCORE 
           MOVE  "20"  TO PROJECT-SCORE 
           WRITE SCORE-DDETAIL 

           MOVE  "62160311"  TO STU-ID
           MOVE  "30"   TO MIDTERM-SCORE 
           MOVE  "10"  TO FINAL-SCORE 
           MOVE  "20"  TO PROJECT-SCORE 
           WRITE SCORE-DDETAIL 

           MOVE  "62160319"  TO STU-ID
           MOVE  "10"   TO MIDTERM-SCORE 
           MOVE  "40"  TO FINAL-SCORE 
           MOVE  "20"  TO PROJECT-SCORE 
           WRITE SCORE-DDETAIL 

           MOVE  "62160146"  TO STU-ID
           MOVE  "10"   TO MIDTERM-SCORE 
           MOVE  "30"  TO FINAL-SCORE 
           MOVE  "20"  TO PROJECT-SCORE 
           WRITE SCORE-DDETAIL  
           CLOSE SCORE-FILE 
           GOBACK 
           .
